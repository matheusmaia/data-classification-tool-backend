from flask import Flask
from flask_migrate import Migrate

from .model import configure as configure_db
from .serializer import configure as configure_serializer

from flask_cors import CORS

def create_app():
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://ewfccnjyxclbwa:53f4d90de1d050b4218c5a8e9f0088402cb21c0b1bafae468e8eb895b192e0d7@ec2-44-193-188-118.compute-1.amazonaws.com:5432/d3jal6hur2hb3d'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    CORS(app)

    configure_db(app)
    configure_serializer(app)

    Migrate(app, app.db)

    from .controller.books import bp_books
    app.register_blueprint(bp_books)

    from .controller.projects import bp_projects
    app.register_blueprint(bp_projects)

    from .controller.variables import bp_variables
    app.register_blueprint(bp_variables)

    from .controller.users import bp_users
    app.register_blueprint(bp_users)
    
    
    return app
