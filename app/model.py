from os import access
from flask_sqlalchemy import SQLAlchemy
from sklearn import datasets

db = SQLAlchemy()


def configure(app):
    db.init_app(app)
    app.db = db


class Book(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), nullable=False)
    author = db.Column(db.String(80), nullable=False)

    def __init__(self, title, author):
        self.title = title
        self.author = author

    def __repr__(self):
        return '<Comentario %r>' % self.id


class Projects(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    label = db.Column(db.String(80))
    description = db.Column(db.String(80))
    author = db.Column(db.String(80))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    dataset_continuous = db.Column(db.Text)
    dataset_discrete = db.Column(db.Text)
    dataset_columns = db.Column(db.ARRAY(db.String(250)))
    dataset_total = db.Column(db.Integer)
    variables_main = db.Column(db.Text)
    variables_label = db.Column(db.Text)
    variables_values = db.Column(db.ARRAY(db.String(250)))
    naivebayes_model = db.Column(db.Text)
    naivebayes_acuraccery = db.Column(db.Float)


    

class Variables(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    label = db.Column(db.String(80))
    project_id = db.Column(db.Integer, db.ForeignKey('projects.id'))
    items = db.Column(db.ARRAY(db.String(80)))
    is_main = db.Column(db.Boolean)


class Users(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    fullname = db.Column(db.String(80))
    email = db.Column(db.String(80))
    accessToken = db.Column(db.String(255))