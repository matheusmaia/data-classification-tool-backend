from sklearn import datasets
from sklearn.naive_bayes import CategoricalNB
import joblib

class ExportNaiveBayes():
    def run(self,   ):
        data = datasets.load_iris()
        clf = CategoricalNB()
        clf.fit(data.data, data.target)
        file = joblib.dump(clf, 'models/naive_bayes.pk1')
        return(file)

exportNaiveBayes = ExportNaiveBayes();