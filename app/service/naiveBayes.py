from lib2to3.pgen2.pgen import DFAState
from flask import jsonify
from sklearn import datasets
from sklearn.naive_bayes import CategoricalNB
import pandas as pd
import json
import csv
import pickle
import codecs


class NaiveBayes():

    def dataContinuousToDiscrete(self, bytes, variables):
        df = pd.DataFrame()
        df = pickle.loads(codecs.decode(bytes.encode(), "base64"))

        for variable in variables:
            for row in df[variable['name']]:
                if type(row) == int:
                    df[variable['name']][df[variable['name']] ==
                                         int(row)] = variable['items'][int(row)]

        serialized = codecs.encode(pickle.dumps(df), "base64").decode()
        return serialized

    def addData(self, bytes, requests):
        df = pd.DataFrame()
        if (bytes):
            df = pickle.loads(codecs.decode(bytes.encode(), "base64"))

        for request in requests:
            df = df.append(request, ignore_index=True)

        rows = df.shape[0]
        columns_name = list(df.columns)

        serialized = codecs.encode(pickle.dumps(df), "base64").decode()
        return serialized, rows, columns_name

    def SerializerDataFrame(self):
        df = pd.read_json('assets/iris.json')
        serialized = codecs.encode(pickle.dumps(df), "base64").decode()
        return serialized

    def DeserializeDataFrame(self, bytes):
        # df = pd.read_json('assets/iris.json')
        df = pickle.loads(codecs.decode(bytes.encode(), "base64"))
        return df.to_json(orient='records')

    def RunBayes(self, projects, variables):

        dfContinuos = pd.DataFrame()

        bytes = projects.dataset_continuous

        
        if(bytes):
            dfContinuos = pickle.loads(codecs.decode(bytes.encode(), "base64"))

        for variable in variables:
            if(variable['is_main'] == True):
                variableKey = variable['name']
                target_names = variable['items']

        for row in dfContinuos[variableKey]:
            if row in target_names:
                dfContinuos[variableKey][dfContinuos[variableKey]
                                         == row] = target_names.index(row)

        data_target = dfContinuos[variableKey].values.astype('int')
        dfContinuos = dfContinuos.drop(variableKey, axis=1)


        clf = CategoricalNB()
        clf.fit(dfContinuos.values, data_target)

        serialized = codecs.encode(pickle.dumps(clf), "base64").decode()
        return serialized

    def predict(self, projects, variables, requests):
        result = {}
        obj = []
        columns = []

        for variable in variables:
            if(variable['is_main'] == True):
                variables_main = variable['name']
                target_names = variable['items']
            else:
                columns.append(variable['name'])

        for column in columns:
            for variable in variables:
                if column == variable['name']:
                    for idx, val in enumerate(variable['items']):
                        if val == requests[column]:
                            obj.append(idx)

        bytes = projects.naivebayes_model
        clf = pickle.loads(codecs.decode(bytes.encode(), "base64"))
        prediction = clf.predict([obj])[0]

        probas = zip(target_names, clf.predict_proba([obj])[0])
        result['prediction'] = target_names[prediction]
        result['probas'] = probas

        return jsonify({'message': result['prediction']})


    def dataDiscreteToContinuous(self, bytes, variables):
        df = pd.DataFrame()
        if(bytes):
            df = pickle.loads(codecs.decode(bytes.encode(), "base64"))

        result = df.to_json(orient="records")
        requests = json.loads(result)

        for request in requests:
            for variable in variables:
                for idx, val in enumerate(variable['items']):
                    if val == request[variable['name']]:
                        request[variable['name']] = idx

        df = pd.DataFrame(requests)

        serialized = codecs.encode(pickle.dumps(df), "base64").decode()
        return serialized

    def PopulateExample(self,):
        df = pd.read_csv('assets/diabetes_tratado.csv')
        # pickled = codecs.encode(pickle.dumps(df), "base64").decode()

        result = df.to_json(orient="records")

        return json.loads(result)

    def PopulateVariables(self,):
        result = [
            {
                "is_main": False,
                "items": [
                    "Maior_375",
                    "Menor_375"
                ],
                "label": "Colesterol",
                "name": "colesterol",
                "project_id": 7
            },
            {
                "is_main": False,
                "items": [
                    "Maior_216",
                    "Menor_216"
                ],
                "label": "Glicose",
                "name": "glicose",
                "project_id": 7
            },
            {
                "is_main": True,
                "items": [
                    "Sim",
                    "Nao"
                ],
                "label": "Diabetes",
                "name": "diabetes",
                "project_id": 7
            },
            {
                "is_main": False,
                "items": [
                    "female",
                    "male"
                ],
                "label": "Genero",
                "name": "genero",
                "project_id": 7
            },
            {
                "is_main": False,
                "items": [
                    "Entre_46_e_68",
                    "Menor46",
                    "Maior68"
                ],
                "label": "Idade",
                "name": "idade",
                "project_id": 7
            },
            {
                "is_main": False,
                "items": [
                    "Maior65",
                    "Menor57",
                    "Entre57_e_65"
                ],
                "label": "Altura",
                "name": "altura",
                "project_id": 7
            }
        ]

        return result


naiveBayes = NaiveBayes()
