import joblib
import pickle


class ImportModel():
    def run(self,):
        result = {}
        event = [0.5, 0.5, 0, 0.5]
        target_names = ['Setosa', 'Versicolor', 'Virginica']

        clf = joblib.load('assets/naive_bayes.pk1')
        prediction = clf.predict([event])[0]
        probas = zip(target_names, clf.predict_proba([event])[0])
        result['prediction'] = target_names[prediction]
        result['probas'] = probas

        return(result)



importModel = ImportModel()
