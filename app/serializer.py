from os import access
from flask_marshmallow import Marshmallow
from app.model import Projects, Book, Users, Variables

ma = Marshmallow()


def configure(app):
    ma.init_app(app)


class BookSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Book
        load_instance = True
        
    id = ma.auto_field()
    title = ma.auto_field()
    author = ma.auto_field()

class ProjectSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Projects
        load_instance = True
    
    id = ma.auto_field()
    name = ma.auto_field()
    label = ma.auto_field()
    user_id = ma.auto_field()
    description = ma.auto_field()
    dataset_discrete = ma.auto_field()
    dataset_continuous = ma.auto_field()
    dataset_columns = ma.auto_field()
    dataset_total = ma.auto_field()
    variables_main = ma.auto_field()
    naivebayes_model = ma.auto_field()
    naivebayes_acuraccery = ma.auto_field()
    variables_label = ma.auto_field()
    variables_values = ma.auto_field()

class VariableSchema(ma.SQLAlchemyAutoSchema):

    class Meta:
        model = Variables
        load_instance = True
    
    id = ma.auto_field()
    name = ma.auto_field()
    label = ma.auto_field()
    project_id = ma.auto_field()
    items = ma.auto_field()

class UsersSchema(ma.SQLAlchemyAutoSchema):

    class Meta:
        model = Users
        load_instance = True
    
    id = ma.auto_field()
    fullname = ma.auto_field()
    email = ma.auto_field()
    accessToken = ma.auto_field()
