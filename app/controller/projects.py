from flask import Blueprint, current_app, jsonify, request
from marshmallow import ValidationError
from sqlalchemy import null

from app.model import Projects, Users, Variables
from app.serializer import ProjectSchema, VariableSchema
from app.service.naiveBayes import naiveBayes

bp_projects = Blueprint('bp_projects', __name__, url_prefix='/projects')


@bp_projects.route('/', methods=['GET'])
def index():
    bs = ProjectSchema(many=True)
    projects = Projects.query.all()
    if projects:
        return jsonify(bs.dump(projects))
    return jsonify({'message': 'No projects found'}), 404


@bp_projects.route('/<int:idenficador>', methods=['GET'])
def get(idenficador):
    bs = ProjectSchema()
    variable = Projects.query.get(idenficador)
    if variable:
        return jsonify(bs.dump(variable))
    return jsonify({'message': 'No Projects found'}), 404


@bp_projects.route('/', methods=['POST'])
def add():
    try:
        bs = ProjectSchema()
        projects = bs.load(request.json)
        user_id = Users.query.get(projects.user_id)
        if not user_id:
            return jsonify({'error': 'Projeto não cadastrado'}), 400

        if projects:
            current_app.db.session.add(projects)
            current_app.db.session.commit()
            return jsonify(bs.dump(projects))
        return jsonify({'error': 'Project not added'}), 400
    except ValidationError as e:
        return jsonify({'error': e.messages}), 400


@bp_projects.route('/<int:idenficador>/', methods=['POST'])
def edit(idenficador):
    try:
        bs = ProjectSchema()
        result = bs.load(request.json)
        projects = Projects.query.get(idenficador)
        if projects:
            projects.name = result.name
            projects.description = result.description
            current_app.db.session.commit()
            return bs.jsonify(result), 200
        return jsonify({'message': 'Bayes not found'}), 404
    except ValidationError as e:
        return jsonify({'error': e.messages}), 400


@bp_projects.route('/<int:idenficador>/', methods=['DELETE'])
def delete(idenficador):
    bs = ProjectSchema()
    bayes = Projects.query.get(idenficador)
    variables = Variables.query.filter_by(project_id=idenficador).all()

    if bayes:
        current_app.db.session.delete(bayes)
        current_app.db.session.commit()
        return bs.jsonify(bayes), 200
    return jsonify({'message': 'Project not found'}), 404


@bp_projects.route('/<int:idenficador>/run/bayes/', methods=['GET'])
def run(idenficador):
    bs = ProjectSchema()
    bsVariable = VariableSchema(many=True)

    variables = bsVariable.dump(Variables.query.filter_by(project_id=idenficador).all())
    projects = Projects.query.get(idenficador)

    if projects:
        projects.dataset_continuous = naiveBayes.dataDiscreteToContinuous( projects.dataset_discrete, variables)
        current_app.db.session.commit()

        projects.naivebayes_model = naiveBayes.RunBayes(projects, variables)
        current_app.db.session.commit()
        return bs.jsonify(projects), 200
    return jsonify({'message': 'Project not found'}), 404


@bp_projects.route('/<int:idenficador>/data_continuos/', methods=['GET'])
def getDataContinuos(idenficador):
    bs = VariableSchema(many=True)
    projects = Projects.query.get(idenficador)
    variables = bs.dump(Variables.query.filter_by(
        project_id=idenficador).all())

    if projects:

        dataFrame = naiveBayes.DeserializeDataFrame(
            projects.dataset_continuous)

        return dataFrame
    return jsonify({'message': 'No Projects found'}), 404


@bp_projects.route('/<int:idenficador>/data', methods=['GET'])
def getData(idenficador):
    bs = VariableSchema(many=True)
    projects = Projects.query.get(idenficador)
    variables = Variables.query.filter_by(project_id=idenficador).all()
    dataFrame = naiveBayes.DeserializeDataFrame(projects.dataset_discrete)

    if projects and dataFrame:
        projects.dataset_continuous = naiveBayes.dataDiscreteToContinuous(
            projects.dataset_discrete, bs.dump(variables))
        return dataFrame
    return jsonify({'message': 'No Projects found'}), 404


@bp_projects.route('/<int:idenficador>/data/', methods=['POST'])
def addRow(idenficador):
    bs = VariableSchema(many=True)
    projects = Projects.query.get(idenficador)
    variables = Variables.query.filter_by(project_id=idenficador).all()

    result = request.json
    if projects:
        projects.dataset_discrete, projects.dataset_total, projects.dataset_columns = naiveBayes.addData(
            projects.dataset_discrete,
            result,
        )

        projects.dataset_continuous = naiveBayes.dataDiscreteToContinuous(
            projects.dataset_discrete, bs.dump(variables))

        current_app.db.session.commit()
        dataFrame = naiveBayes.DeserializeDataFrame(projects.dataset_discrete)
        return dataFrame, 200
    return jsonify({'message': 'Project not found'}), 404


@bp_projects.route('/<int:idenficador>/run/predict', methods=['POST'])
def predict(idenficador):
    bs = VariableSchema(many=True)
    result = request.json
    projects = Projects.query.get(idenficador)
    variables = Variables.query.filter_by(project_id=idenficador).all()

    if not projects.naivebayes_model:
        return jsonify({'message': 'Model not found'}), 404

    if projects:
        predict = naiveBayes.predict(projects, bs.dump(variables), result)
        return predict, 200
    return jsonify({'message': 'Project not found'}), 404


@bp_projects.route('/<int:idenficador>/variables/', methods=['GET'])
def changeItems(idenficador):
    bs = VariableSchema(many=True)
    variables = Variables.query.filter_by(project_id=idenficador).all()

    if variables:
        return jsonify(bs.dump(variables)), 200
    return jsonify({'message': 'project not found'}), 404


@bp_projects.route('/<int:idenficador>/setVariableMain/', methods=['POST'])
def setVariableMain(idenficador):
    result = request.json
    bs = ProjectSchema()
    variables = Variables.query.filter_by(project_id=idenficador).all()
    projects = Projects.query.get(idenficador)

    if projects:
        for variable in variables:
            variable.is_main = False
            if variable.id == result['id']:
                variable.is_main = True
                projects.variables_label = variable.label
                projects.variables_main = variable.name
                projects.variables_values = variable.items
        current_app.db.session.commit()
        return bs.jsonify(projects), 200

    return jsonify({'message': 'Project not found'}), 404


@bp_projects.route('/<int:idenficador>/test/', methods=['GET'])
def test(idenficador):
    bs = ProjectSchema()
    project = Projects.query.get(idenficador)

    if not project:
        return jsonify({'error': 'Projeto não cadastrado'}), 400

    populateVariables(idenficador)
    populateRows(idenficador)
    return jsonify(bs.dump(project)), 200


def populateRows(idenficador):
    project_mock = naiveBayes.PopulateExample()

    bs = VariableSchema(many=True)
    project = Projects.query.get(idenficador)
    variables = Variables.query.filter_by(project_id=idenficador).all()

    project.dataset_discrete, project.dataset_total, project.dataset_columns = naiveBayes.addData(
        False,
        project_mock)

    project.dataset_continuous = naiveBayes.dataDiscreteToContinuous(
        project.dataset_discrete, bs.dump(variables))

    current_app.db.session.commit()


def populateVariables(idenficador):
    variable_mock = naiveBayes.PopulateVariables()
    bs = VariableSchema()

    Variables.query.filter_by(project_id=idenficador).delete()

    for variable in variable_mock:
        variable = bs.load(variable)
        variable.project_id = idenficador
        if variable:
            current_app.db.session.add(variable)
            current_app.db.session.commit()


def starterProject(user_id):
    try:
        starterProject = {
            "author": "Matheus Maia",
            "description": "Teste Diabetes",
            "label": "Diabetes",
            "name": "diabetes",
            "user_id": user_id
        }

        bs = ProjectSchema()
        projects = bs.load(starterProject)
        if projects:
            current_app.db.session.add(projects)
            current_app.db.session.commit()

            populateVariables(projects.id)
            populateRows(projects.id)   

            return jsonify(bs.dump(projects))
        return jsonify({'error': 'Project not added'}), 400
    except ValidationError as e:
        return jsonify({'error': e.messages}), 400

    
