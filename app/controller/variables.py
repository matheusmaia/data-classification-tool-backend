from flask import Blueprint, current_app, jsonify, request
from marshmallow import ValidationError
from sqlalchemy import false

from app.model import Variables, Projects
from ..serializer import ProjectSchema, VariableSchema

bp_variables = Blueprint('bp_variables', __name__, url_prefix='/variables')


@bp_variables.route('/', methods=['GET'])
def index():
    bs = VariableSchema(many=True)
    variables = Variables.query.all()
    if variables:
        return jsonify(bs.dump(variables))
    return jsonify({'message': 'No variables found'}), 404


@bp_variables.route('/<int:idenficador>', methods=['GET'])
def get(idenficador):
    bs = VariableSchema()
    variable = Variables.query.get(idenficador)
    if variable:
        return jsonify(bs.dump(variable))
    return jsonify({'message': 'No variables found'}), 404


@bp_variables.route('/', methods=['POST'])
def add():
    try:
        bs = VariableSchema()
        variable = bs.load(request.json)
        project_id = Projects.query.get(variable.project_id)
        if not project_id:
            return jsonify({'error': 'Projeto não cadastrado'}), 400

        if variable:
            current_app.db.session.add(variable)
            current_app.db.session.commit()
            return jsonify(bs.dump(variable)), 201
        return jsonify({'error': 'variable not added'}), 400
    except ValidationError as e:
        return jsonify({'error': e.messages}), 400


@bp_variables.route('/<int:idenficador>', methods=['POST'])
def edit(idenficador):
    bs = VariableSchema()
    result = bs.load(request.json)
    variable = Variables.query.get(idenficador)
    if variable:
        variable.name = result.name
        variable.items = result.items
        variable.label = result.label

        current_app.db.session.commit()
        return bs.jsonify(result), 200
    return jsonify({'message': 'variable not found'}), 404


@bp_variables.route('/<int:idenficador>/items', methods=['PATCH'])
def changeItems(idenficador):
    bs = VariableSchema()
    result = bs.load(request.json)
    variable = Variables.query.get(idenficador)
    if variable:
        variable.items = result.items
        current_app.db.session.commit()
        return bs.jsonify(variable), 200
    return jsonify({'message': 'variable not found'}), 404


@bp_variables.route('/<int:idenficador>', methods=['DELETE'])
def delete(idenficador):
    bs = VariableSchema()
    variable = Variables.query.get(idenficador)

    if variable:
        current_app.db.session.delete(variable)
        current_app.db.session.commit()
        return bs.jsonify(variable), 200
    return jsonify({'message': 'variable not found'}), 404


@bp_variables.route('/<int:idenficador>/setMain', methods=['GET'])
def setMain(idenficador):
    bs = VariableSchema(many=True)
    variables = Variables.query.all()
    variableCheck = Variables.query.get(idenficador)
    if variableCheck:
        for variable in variables:
            variable.is_main = False
            if variable.id == idenficador:
                variable.is_main = True
        current_app.db.session.commit()
        return bs.jsonify(variables), 200
    return jsonify({'message': 'variable not found'}), 404


@bp_variables.route('/<int:idenficador>/test/', methods=['GET'])
def test(idenficador):
    try:

        project_id = Projects.query.get(idenficador)
        if not project_id:
            return jsonify({'error': 'Projeto não cadastrado'}), 400

        variable_mock = [
            {
                "is_main": False,
                "items": [
                    "Maior_375",
                    "Menor_375"
                ],
                "label": "Colesterol",
                "name": "colesterol",
                "project_id": 6
            },
            {
                "is_main": False,
                "items": [
                    "Maior_216",
                    "Menor_216"
                ],
                "label": "Glicose",
                "name": "glicose",
                "project_id": 6
            },
            {
                "is_main": False,
                "items": [
                    "Maior_46",
                    "Menor_46"
                ],
                "label": "Idade",
                "name": "idade",
                "project_id": 6
            },
            {
                "is_main": False,
                "items": [
                    "famale",
                    "male"
                ],
                "label": "Genero",
                "name": "genero",
                "project_id": 6
            },
            {
                "is_main": False,
                "items": [
                    "Maior65",
                    "Menor65",
                    "Entre57_e_65"
                ],
                "label": "Altura",
                "name": "altura",
                "project_id": 6
            },
            {
                "is_main": True,
                "items": [
                    "Sim",
                    "Nao"
                ],
                "label": "Diabetes",
                "name": "diabetes",
                "project_id": 6
            }
        ]

        for variable in variable_mock:
            populateVariables(variable)

        return jsonify({'error': 'sucess'}), 201

    except ValidationError as e:
        return jsonify({'error': e.messages}), 400


def populateVariables(variable_mock):
    bs = VariableSchema()
    variable = bs.load(variable_mock)

    if variable:
        current_app.db.session.add(variable)
        current_app.db.session.commit()
