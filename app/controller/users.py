from flask import Blueprint, current_app, jsonify, request
from marshmallow import ValidationError
from app.controller.projects import populateRows, populateVariables, starterProject

from app.model import Users, Projects
from ..serializer import UsersSchema, ProjectSchema

bp_users = Blueprint('bp_users', __name__, url_prefix='/users')


@bp_users.route('', methods=['GET'])
def index():
    bs = UsersSchema(many=True)
    users = Users.query.all()
    if users:
        return jsonify(bs.dump(users))
    return jsonify({'message': 'No users found'}), 404


@bp_users.route('/<int:idenficador>', methods=['GET'])
def get(idenficador):
    bs = UsersSchema()
    user = Users.query.get(idenficador)
    if user:
        return jsonify(bs.dump(user))
    return jsonify({'message': 'No users found'}), 404


@bp_users.route('', methods=['POST'])
def add():
    try:
        bs = UsersSchema()
        user = bs.load(request.json)
        if user:
            current_app.db.session.add(user)
            current_app.db.session.commit()

            starterProject(bs.dump(user)['id'])
            return jsonify(bs.dump(user)), 201
        return jsonify({'error': 'user not added'}), 400
    except ValidationError as e:
        return jsonify({'error': e.messages}), 400


@bp_users.route('/<int:idenficador>', methods=['POST'])
def edit(idenficador):
    bs = UsersSchema()
    result = bs.load(request.json)
    user = Users.query.get(idenficador)
    if result.fullname and result.email and user:
        user.fullname = result.fullname
        user.email = result.email
        current_app.db.session.commit()
        return bs.jsonify(user), 200
    return jsonify({'message': 'user not found'}), 404


@bp_users.route('/<int:idenficador>/projects', methods=['GET'])
def changeItems(idenficador):
    bs = ProjectSchema(many=True)
    projects = Projects.query.filter_by(user_id=idenficador).all()
    if projects:
        return jsonify(bs.dump(projects)), 200
    return jsonify({'message': 'user not found'}), 404


@bp_users.route('/<int:idenficador>', methods=['DELETE'])
def delete(idenficador):
    bs = UsersSchema()
    user = Users.query.get(idenficador)
    if user:
        current_app.db.session.delete(user)
        current_app.db.session.commit()
        return bs.jsonify(user), 200
    return jsonify({'message': 'user not found'}), 404


@bp_users.route('/login', methods=['POST'])
def login():
    result = request.json
    bs = UsersSchema()
    user = Users.query.filter_by(email=result['email']).first()

    if user:
        return jsonify(bs.dump(user)), 200
    return jsonify({'message': 'user not found'}), 404
