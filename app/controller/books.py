from flask import Blueprint, current_app, jsonify, request

from app.model import Book
from ..serializer import BookSchema

bp_books = Blueprint('bp_books', __name__, url_prefix='/books')


@bp_books.route('/', methods=['GET'])
def index():
    bs = BookSchema(many=True)
    books = Book.query.all()
    if books:
        return jsonify(bs.dump(books))
    return jsonify({'message': 'No books found'}), 404


@bp_books.route('/add', methods=['POST'])
def add():
    bs = BookSchema()
    book = bs.load(request.json)
    if book:
        current_app.db.session.add(book)
        current_app.db.session.commit()
        return jsonify(bs.dump(book))
    return jsonify({'error': 'Book not added'}), 400


@bp_books.route('/<int:idenficador>/edit', methods=['POST'])
def edit(idenficador):
    bs = BookSchema()
    result = bs.load(request.json)
    book = Book.query.get(idenficador)
    if book:
        book.title = result.title
        book.author = result.author
        current_app.db.session.commit()
        return bs.jsonify(result), 200
    return jsonify({'message': 'Book not found'}), 404


@bp_books.route('/<int:idenficador>/delete', methods=['GET', 'POST'])
def delete(idenficador):
    bs = BookSchema()
    book = Book.query.get(idenficador)
    if book:
        current_app.db.session.delete(book)
        current_app.db.session.commit()
        return bs.jsonify(book), 200
    return jsonify({'message': 'Book not found'}), 404
