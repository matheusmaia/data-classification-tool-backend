# Python Basic Comands

- Install dependencies
```
pip install -r requirements.txt
```

- Update dependencies
```
pip freeze > requirements.txt
```

- Active virtual environment
```
. .\.venv\Scripts\Activate.ps1
```

- Development mode
```	
$env:FLASK_ENV = "development"
```


- Run Flask
```
 flask run
```

# Heroku Basic Comands

- Create new app
```
heroku create 
```

- push to heroku
```
git push heroku main
```

- Run local server
```
heroku local
```


- Heroku Postgres DB Credentials
```PowerShell
 heroku pg:credentials:url
```

- Heroku Logs
```PowerShell
    heroku logs --tail
```


# Migrate

- Flask DB Init
```PowerShell
 flask db init
```

- Flask DB Migrate
```PowerShell
 flask db migrate   
```

- Flask DB Upgrade
```PowerShell
 flask db upgrade
```

- Flask DB Downgrade
```PowerShell
 flask db downgrade 
```
